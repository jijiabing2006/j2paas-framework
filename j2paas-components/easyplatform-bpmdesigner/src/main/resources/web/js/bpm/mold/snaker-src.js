function (out){
	out.push('<div id="',this.uuid,'" class="',this.getZclass(),'">');
	out.push('<div id="',this.uuid,'-canvas"','/>');
	if(this._editable){
		var ctx = zk.ajaxURI('/web/js/bpm/ext/snaker/', {
			desktop : this.desktop,
			au : true
		});
		var ctx = ctx.substr(0, ctx.lastIndexOf("/") + 1);
		out.push('<div id="',this.uuid,'-toolbox" ','class="z-popup z-snaker-toolbox">');
		out.push('<div id="',this.uuid,'-toolbox-handle"','class="z-toolbar">',msgepez['BPM_TOOLBAR'],'</div>');
		//save
		out.push('<div id="',this.uuid,'-save" ','class="z-snaker-node"','<div>');
		out.push('<img src="',ctx,'images/save.gif"/>');
		out.push('&nbsp;&nbsp;',msgepez['BPM_SAVE'],'</div>');
		//bar
		out.push('<div><hr class="z-snaker-sep"/></div>');
		//pointer
		out.push('<div id="',this.uuid,'-pointer" ','class="z-snaker-node selectable" title="',msgepez['BPM_SELECT_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/select16.gif"/>');
		out.push('&nbsp;&nbsp;Select','</div>');
		//path
		out.push('<div id="',this.uuid,'-path" ','class="z-snaker-node selectable" title="',msgepez['BPM_TRANSITION_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/flow_sequence.png"/>');
		out.push('&nbsp;&nbsp;transition','</div>');
		//bar
		out.push('<div><hr class="z-snaker-sep"/></div>');
		//start
		out.push('<div id="',this.uuid,'-start" ','class="z-snaker-node state" ','type="start" title="',msgepez['BPM_START_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/start_event_empty.png"/>');
		out.push('&nbsp;&nbsp;start','</div>');
		//end
		out.push('<div id="',this.uuid,'-end" ','class="z-snaker-node state" ','type="end" title="',msgepez['BPM_END_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/end_event_terminate.png"/>');
		out.push('&nbsp;&nbsp;end','</div>');
		//task
		out.push('<div id="',this.uuid,'-task" ','class="z-snaker-node state" ','type="task" title="',msgepez['BPM_TASK_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/task_empty.png"/>');
		out.push('&nbsp;&nbsp;task','</div>');
		//subprocess
		out.push('<div id="',this.uuid,'-task" ','class="z-snaker-node state" ','type="subprocess" title="',msgepez['BPM_SUBPROCESS_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/task_empty.png"/>');
		out.push('&nbsp;&nbsp;subprocess','</div>');
		//decision
		out.push('<div id="',this.uuid,'-fork" ','class="z-snaker-node state" ','type="decision" title="',msgepez['BPM_DECISION_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/gateway_exclusive.png"/>');
		out.push('&nbsp;&nbsp;decision','</div>');
		//fork
		out.push('<div id="',this.uuid,'-fork" ','class="z-snaker-node state" ','type="fork" title="',msgepez['BPM_FORK_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/gateway_parallel.png"/>');
		out.push('&nbsp;&nbsp;fork','</div>');
		//join
		out.push('<div id="',this.uuid,'-join" ','class="z-snaker-node state" ','type="join" title="',msgepez['BPM_JOIN_TITLE'],'"','<div>');
		out.push('<img src="',ctx,'images/16/end_event_error.png"/>');
		out.push('&nbsp;&nbsp;join','</div>');
		//toolbox结束
		out.push('</div>');
	}
	//properties开始
	out.push('<div id="',this.uuid,'-properties" ','class="z-popup z-snaker-properties">');
	out.push('<div id="',this.uuid,'-properties-handle" ','class="z-toolbar"','>',msgepez['BPM_PROPERTIES'],'</div>');
	out.push('<table ','class="z-snaker-table"','/>');
	out.push('<div>&nbsp;</div>');
	//properties结束
	out.push('</div>');
	//root结束
	out.push('</div>');
}