function (out) {

	out.push('<div ', this.domAttrs_(), 'id="',this.uuid,'"  style="width:100%;height:100%;position:relative">');
	out.push('<div id="',this.uuid,'-canvas" style="width:100%;height:100%;padding:0;margin:0;border: solid 1px #dddddd;"  />');
    out.push('<button id="save-button" class="j2pass-bpmn-button"  onclick="zk.$(\'', this.uuid, '\').save_()">保存</button>');
    out.push('<div id="',this.uuid,'-Panel" class="j2pass-bpmn-Panel" >');
    out.push('<div class="j2pass-bpmn-Panel-toggle-div" draggable="true" onclick="zk.$(\'', this.uuid, '\').togglePanelDiv_(true)">Properties Panel</div>');
    out.push('<div class="j2pass-bpmn-Panel-north">');
    out.push('<span id="',this.uuid,'-label" class="j2pass-bpmn-title1-label">Element</span>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-center">');
    out.push('<span id="',this.uuid,'-rightArrow" class="j2pass-bpmn-right-arrow">></span>');
    out.push('<span id="',this.uuid,'-leftArrow" class="j2pass-bpmn-left-arrow"><</span>');
    out.push('<ul class="j2pass-bpmn-ul">');
    out.push('<li class="j2pass-bpmn-li" id="',this.uuid,'-general-li" onclick="zk.$(\'', this.uuid, '\').tabChange_(this)">');
    out.push('<a class="j2pass-bpmn-a-select">常规</a>');
    out.push('</li>');
    out.push('<li class="j2pass-bpmn-li" id="',this.uuid,'-property-li" onclick="zk.$(\'', this.uuid, '\').tabChange_(this)">');
    out.push('<a class="j2pass-bpmn-a-unselect">扩展属性</a>');
    out.push('</li>');
    out.push('</ul>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-south" id="',this.uuid,'-generalDiv">');
    out.push('<span class="j2pass-bpmn-title2-label">基础属性</span>');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label" >ID</span>');
    out.push('<input id="',this.uuid,'-id" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label">名称</span>');
    out.push('<input id="',this.uuid,'-name" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');

    //点击甬道时的属性
    out.push('<div id="',this.uuid,'-processDiv" >');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label" >流程ID</span>');
    out.push('<input id="',this.uuid,'-processId" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label">流程名称</span>');
    out.push('<input id="',this.uuid,'-processName" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label">版本标签</span>');
    out.push('<input id="',this.uuid,'-version" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('<div class="j2pass-bpmn-Panel-center-check-div">');
    out.push('<input id="',this.uuid,'-executable" type="checkbox" class="j2pass-bpmn-checkbox" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('<label for="',this.uuid,'-executable" class="j2pass-bpmn-title4-label">Executable</label>');
    out.push('</div>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-center-div">');
    out.push('<span class="j2pass-bpmn-title2-label" >拓展任务配置</span>');
    out.push('<span class="j2pass-bpmn-title3-label">任务优先级</span>');
    out.push('<input id="',this.uuid,'-task" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-center-div">');
    out.push('<span class="j2pass-bpmn-title2-label">工作配置</span>');
    out.push('<span class="j2pass-bpmn-title3-label">工作优先级</span>');
    out.push('<input id="',this.uuid,'-work" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('</div>');

    //点击节点时的属性
    out.push('<div id="',this.uuid,'-taskDiv" style="display: none">');
    out.push('<div class="j2pass-bpmn-Panel-center-div" >');
    out.push('<span class="j2pass-bpmn-title2-label">异步连续</span>');
    out.push('<div class="j2pass-bpmn-Panel-center-check-div">');
    out.push('<input id="',this.uuid,'-asynchronous-before" type="checkbox" class="j2pass-bpmn-checkbox" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('<label for="',this.uuid,'-asynchronous-before" class="j2pass-bpmn-title4-label">Asynchronous Befor</label>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-center-check-div">');
    out.push('<input id="',this.uuid,'-asynchronous-after" type="checkbox" class="j2pass-bpmn-checkbox" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('<label for="',this.uuid,'-asynchronous-after" class="j2pass-bpmn-title4-label">Asynchronous After</label>');
    out.push('</div>');
    out.push('</div>');
    out.push('<div class="j2pass-bpmn-Panel-center-div" >');
    out.push('<span class="j2pass-bpmn-title2-label">文档</span>');
    out.push('<span class="j2pass-bpmn-title3-label">节点资料文档</span>');
    out.push('<input id="',this.uuid,'-documentation" type="text"  class="j2pass-bpmn-input" onchange="zk.$(\'',this.uuid,'\').changeProperty_(this)"/>');
    out.push('</div>');
    out.push('</div>');

    out.push('</div>');

    //点击拓展属性时的属性
    out.push('<div id="',this.uuid,'-propertyDiv" class="j2pass-bpmn-Panel-south" style="display: none">');
    out.push('<span class="j2pass-bpmn-title2-label">属性</span>');
    out.push('<div style="margin-top: 8px;">');
    out.push('<span class="j2pass-bpmn-title3-label" >添加自定义属性</span>');
    out.push('<button id="',this.uuid,'-addPropertyButton" class="j2pass-bpmn-panel-property-button" onclick="zk.$(\'',this.uuid,'\').addPropertyDiv_(\'\',\'\')">');
    out.push('<span class="j2pass-bpmn-panel-property-span">+</span>');
    out.push('</button>');
    out.push('<div id="',this.uuid,'-propertyContentDiv" style="height:470px;overflow:auto">');
    out.push('</div>');
    out.push('</div>');
    out.push('</div>');

    out.push('</div>');
	out.push('</div>');

}