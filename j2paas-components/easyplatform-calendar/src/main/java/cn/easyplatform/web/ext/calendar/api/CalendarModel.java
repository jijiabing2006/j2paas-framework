/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.api;

import java.util.Date;
import java.util.List;

import cn.easyplatform.web.ext.calendar.event.CalendarDataListener;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface CalendarModel {
	/**
	 * Returns the list that must be a list of {@link CalendarEvent} type.
	 * 
	 * @param beginDate the begin date
	 * @param endDate the end date
	 * @param rc a RenderContext encapsulates the information needed for Calendars.
	 */
	public List<CalendarEvent> get(Date beginDate, Date endDate, RenderContext rc);	
	/** Adds a listener to the calendar model that's notified each time a change
	 * to the data model occurs. 
	 */
	public void addCalendarDataListener(CalendarDataListener l);
    /** Removes a listener from the calendar model that's notified each time
     * a change to the data model occurs. 
     */
	public void removeCalendarDataListener(CalendarDataListener l) ;
	
	/**
	 * @param id
	 * @return
	 */
	public CalendarEvent getEventById(Object id);
}
