/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.builder;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class EChartsTemplateHandler extends SelectorComposer<Component> {

    @Listen("onClick = button")
    public void onHook(Event evt) {
        EChartsBuilderController builder = (EChartsBuilderController) evt.getTarget().getRoot().getAttribute("$composer");
        String name = (String) evt.getTarget().getAttribute("name");
        if (evt.getTarget().getId().equals("view"))
            builder.onViewTemplate(evt.getTarget(), name);
        else if (evt.getTarget().getId().equals("data")) {
            builder.onDataTemplate(evt.getTarget(), name);
        } else
            builder.onApplyTemplate(evt.getTarget(), name);
    }
}
