/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class LinesData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    /**
     * 名称
     */
    private String name;
    private String fromName;
    private String toName;
    private List<Object> coords = new ArrayList<Object>();
    private ItemStyle label;
    private ItemStyle lineStyle;

    public String name() {
        return this.name;
    }

    public LinesData name(String name) {
        this.name = name;
        return this;
    }

    public String fromName() {
        return this.fromName;
    }

    public LinesData fromName(String fromName) {
        this.fromName = fromName;
        return this;
    }

    public String toName() {
        return this.toName;
    }

    public LinesData toName(String toName) {
        this.toName = toName;
        return this;
    }

    public List<Object> coords() {
        return this.coords;
    }

    public LinesData coords(Object value) {
        this.coords.add(value);
        return this;
    }

    public LinesData coords(Object... value) {
        this.coords.addAll(Arrays.asList(value));
        return this;
    }

    public ItemStyle label() {
        if (label == null)
            label = new ItemStyle();
        return this.label;
    }

    public LinesData label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new ItemStyle();
        return this.lineStyle;
    }

    public LinesData lineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Object> getCoords() {
        return coords;
    }

    public void setCoords(List<Object> coords) {
        this.coords = coords;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public ItemStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(ItemStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }
}
