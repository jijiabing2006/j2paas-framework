/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.graphic;

import cn.easyplatform.web.ext.echarts.lib.graphic.style.GraphicStyle;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Ring extends Graphic {
    private Shape shape;

    private GraphicStyle style;

    public Ring() {
        this.type("ring");
    }

    public Shape shape() {
        if (shape == null)
            shape = new Shape();
        return shape;
    }

    public Ring shape(Shape shape) {
        this.shape = shape;
        return this;
    }

    public GraphicStyle style() {
        if (style == null)
            style = new GraphicStyle();
        return style;
    }

    public Ring style(GraphicStyle style) {
        this.style = style;
        return this;
    }

    public static class Shape {
        /**
         * 图形元素的中心在父节点坐标系（以父节点左上角为原点）中的横坐标值
         */
        private Integer cx;
        /**
         * 图形元素的中心在父节点坐标系（以父节点左上角为原点）中的纵坐标值
         */
        private Integer cy;
        /**
         * 外半径
         */
        private Integer r;
        /**
         * 内半径
         */
        private Integer r0;

        public Integer r() {
            return this.r;
        }

        public Shape r(Integer r) {
            this.r = r;
            return this;
        }
        public Integer r0() {
            return this.r0;
        }

        public Shape r0(Integer r0) {
            this.r0 = r0;
            return this;
        }
        public Integer cy() {
            return this.cy;
        }

        public Shape cy(Integer cy) {
            this.cy = cy;
            return this;
        }

        public Integer cx() {
            return this.cx;
        }

        public Shape cx(Integer cx) {
            this.cx = cx;
            return this;
        }

        public Integer getCx() {
            return cx;
        }

        public void setCx(Integer cx) {
            this.cx = cx;
        }

        public Integer getCy() {
            return cy;
        }

        public void setCy(Integer cy) {
            this.cy = cy;
        }

        public Integer getR() {
            return r;
        }

        public void setR(Integer r) {
            this.r = r;
        }
    }
}
