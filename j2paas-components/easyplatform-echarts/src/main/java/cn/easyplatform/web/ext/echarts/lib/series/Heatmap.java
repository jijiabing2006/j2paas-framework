/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series;

import cn.easyplatform.web.ext.echarts.lib.Tooltip;
import cn.easyplatform.web.ext.echarts.lib.support.MarkArea;
import cn.easyplatform.web.ext.echarts.lib.support.MarkLine;
import cn.easyplatform.web.ext.echarts.lib.support.MarkPoint;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Heatmap implements Serializable {
    private String type = "heatmap";
    private String name;
    private String coordinateSystem;
    private Integer xAxisIndex;
    private Integer yAxisIndex;
    private Integer geoIndex;
    private Integer blurSize;
    private Object minOpacity;
    private Object maxOpacity;
    private List<Object> data;
    private MarkPoint markPoint;
    private MarkLine markLine;
    private MarkArea markArea;
    private Integer zlevel;
    private Integer z;
    private Boolean silent;
    private Tooltip tooltip;
    private Integer pointSize;
    private Integer calendarIndex;
    /**
     * 渐进式渲染时每一帧绘制图形数量，设为 0 时不启用渐进式渲染，支持每个系列单独配置
     */
    private Object progressive;
    /**
     * 启用渐进式渲染的图形数量阈值，在单个系列的图形数量超过该阈值时启用渐进式渲染。
     */
    private Object progressiveThreshold;

    public Object progressiveThreshold() {
        return progressiveThreshold;
    }

    public Heatmap progressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
        return this;
    }

    public Object progressive() {
        return progressive;
    }

    public Heatmap progressive(Object progressive) {
        this.progressive = progressive;
        return this;
    }

    public Integer calendarIndex() {
        return calendarIndex;
    }

    public Heatmap calendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
        return this;
    }

    public Integer pointSize() {
        return pointSize;
    }

    public Heatmap pointSize(Integer pointSize) {
        this.pointSize = pointSize;
        return this;
    }

    public Tooltip tooltip() {
        if (tooltip == null)
            tooltip = new Tooltip();
        return tooltip;
    }

    public Heatmap tooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
        return this;
    }

    public String type() {
        return type;
    }

    public Heatmap type(String type) {
        this.type = type;
        return this;
    }

    public String name() {
        return name;
    }

    public Heatmap name(String name) {
        this.name = name;
        return this;
    }

    public String coordinateSystem() {
        return coordinateSystem;
    }

    public Heatmap coordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
        return this;
    }

    public Integer xAxisIndex() {
        return xAxisIndex;
    }

    public Heatmap xAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Integer yAxisIndex() {
        return yAxisIndex;
    }

    public Heatmap yAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
        return this;
    }

    public Integer geoIndex() {
        return geoIndex;
    }

    public Heatmap geoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Integer blurSize() {
        return blurSize;
    }

    public Heatmap blurSize(Integer blurSize) {
        this.blurSize = blurSize;
        return this;
    }

    public Object minOpacity() {
        return minOpacity;
    }

    public Heatmap minOpacity(Object minOpacity) {
        this.minOpacity = minOpacity;
        return this;
    }

    public Object maxOpacity() {
        return maxOpacity;
    }

    public Heatmap maxOpacity(Object maxOpacity) {
        this.maxOpacity = maxOpacity;
        return this;
    }

    public List<Object> data() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public Heatmap data(Object... values) {
        if (data == null)
            data = new ArrayList<Object>();
        this.data.add(values);
        return this;
    }

    public MarkPoint markPoint() {
        if (markPoint == null)
            markPoint = new MarkPoint();
        return markPoint;
    }

    public Heatmap markPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
        return this;
    }

    public MarkLine markLine() {
        if (markLine == null)
            markLine = new MarkLine();
        return markLine;
    }

    public Heatmap markLine(MarkLine markLine) {
        this.markLine = markLine;
        return this;
    }

    public MarkArea markArea() {
        if (markArea == null)
            markArea = new MarkArea();
        return markArea;
    }

    public Heatmap markArea(MarkArea markArea) {
        this.markArea = markArea;
        return this;
    }

    public Heatmap zlevel(Integer zlevel) {
        this.zlevel = zlevel;
        return this;
    }

    public Integer zlevel() {
        return this.zlevel;
    }

    public Heatmap z(Integer z) {
        this.z = z;
        return this;
    }

    public Integer z() {
        return this.z;
    }

    public Boolean silent() {
        return silent;
    }

    public Heatmap silent(Boolean silent) {
        this.silent = silent;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCoordinateSystem() {
        return coordinateSystem;
    }

    public void setCoordinateSystem(String coordinateSystem) {
        this.coordinateSystem = coordinateSystem;
    }

    public Integer getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Integer xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Integer getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Integer yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public Integer getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Integer geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Integer getBlurSize() {
        return blurSize;
    }

    public void setBlurSize(Integer blurSize) {
        this.blurSize = blurSize;
    }

    public Object getMinOpacity() {
        return minOpacity;
    }

    public void setMinOpacity(Object minOpacity) {
        this.minOpacity = minOpacity;
    }

    public Object getMaxOpacity() {
        return maxOpacity;
    }

    public void setMaxOpacity(Object maxOpacity) {
        this.maxOpacity = maxOpacity;
    }

    public List<Object> getData() {
        if (data == null)
            data = new ArrayList<Object>();
        return data;
    }

    public void setData(List<Object> data) {
        this.data = data;
    }

    public MarkPoint getMarkPoint() {
        return markPoint;
    }

    public void setMarkPoint(MarkPoint markPoint) {
        this.markPoint = markPoint;
    }

    public MarkLine getMarkLine() {
        return markLine;
    }

    public void setMarkLine(MarkLine markLine) {
        this.markLine = markLine;
    }

    public MarkArea getMarkArea() {
        return markArea;
    }

    public void setMarkArea(MarkArea markArea) {
        this.markArea = markArea;
    }

    public Integer getZlevel() {
        return zlevel;
    }

    public void setZlevel(Integer zlevel) {
        this.zlevel = zlevel;
    }

    public Integer getZ() {
        return z;
    }

    public void setZ(Integer z) {
        this.z = z;
    }

    public Boolean getSilent() {
        return silent;
    }

    public void setSilent(Boolean silent) {
        this.silent = silent;
    }

    public Tooltip getTooltip() {
        return tooltip;
    }

    public void setTooltip(Tooltip tooltip) {
        this.tooltip = tooltip;
    }

    public Integer getPointSize() {
        return pointSize;
    }

    public void setPointSize(Integer pointSize) {
        this.pointSize = pointSize;
    }

    public Integer getCalendarIndex() {
        return calendarIndex;
    }

    public void setCalendarIndex(Integer calendarIndex) {
        this.calendarIndex = calendarIndex;
    }

    public Object getProgressive() {
        return progressive;
    }

    public void setProgressive(Object progressive) {
        this.progressive = progressive;
    }

    public Object getProgressiveThreshold() {
        return progressiveThreshold;
    }

    public void setProgressiveThreshold(Object progressiveThreshold) {
        this.progressiveThreshold = progressiveThreshold;
    }
}
