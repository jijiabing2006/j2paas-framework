/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.series.base;

import java.io.Serializable;

/**
 * 图形炫光特效
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Effect implements Serializable {

    private static final long serialVersionUID = 2768587032945006946L;

    private Boolean show;

    private Boolean loop;

    private Integer period;

    private Integer constantSpeed;

    private String color;

    private Object symbol;

    private Object symbolSize;

    private Double trailLength;

    private Object delay;

    public Object delay() {
        return this.delay;
    }

    public Effect delay(Object delay) {
        this.delay = delay;
        return this;
    }

    public Integer constantSpeed() {
        return this.constantSpeed;
    }

    public Effect constantSpeed(Integer constantSpeed) {
        this.constantSpeed = constantSpeed;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public Effect show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean loop() {
        return this.loop;
    }

    public Effect loop(Boolean loop) {
        this.loop = loop;
        return this;
    }

    public Integer period() {
        return this.period;
    }

    public Effect period(Integer period) {
        this.period = period;
        return this;
    }

    public String color() {
        return this.color;
    }

    public Effect color(String color) {
        this.color = color;
        return this;
    }

    public Object symbol() {
        return this.symbol;
    }

    public Effect symbol(Object symbol) {
        this.symbol = symbol;
        return this;
    }

    public Object symbolSize() {
        return this.symbolSize;
    }

    public Effect symbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Effect symbolSize(Object[] symbolSize) {
        this.symbolSize = symbolSize;
        return this;
    }

    public Effect symbolSize(Object width, Object height) {
        this.symbolSize = new Object[]{width, height};
        return this;
    }

    public Double trailLength() {
        return this.trailLength;
    }

    public Effect trailLength(Double trailLength) {
        this.trailLength = trailLength;
        return this;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public Boolean getLoop() {
        return loop;
    }

    public void setLoop(Boolean loop) {
        this.loop = loop;
    }

    public Integer getPeriod() {
        return period;
    }

    public void setPeriod(Integer period) {
        this.period = period;
    }

    public Integer getConstantSpeed() {
        return constantSpeed;
    }

    public void setConstantSpeed(Integer constantSpeed) {
        this.constantSpeed = constantSpeed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Object getSymbol() {
        return symbol;
    }

    public void setSymbol(Object symbol) {
        this.symbol = symbol;
    }

    public Object getSymbolSize() {
        return symbolSize;
    }

    public void setSymbolSize(Object symbolSize) {
        this.symbolSize = symbolSize;
    }

    public Double getTrailLength() {
        return trailLength;
    }

    public void setTrailLength(Double trailLength) {
        this.trailLength = trailLength;
    }

    public Object getDelay() {
        return delay;
    }

    public void setDelay(Object delay) {
        this.delay = delay;
    }
}
