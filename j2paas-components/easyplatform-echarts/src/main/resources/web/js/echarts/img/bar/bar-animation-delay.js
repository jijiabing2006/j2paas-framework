{
    title: {
        text: '柱状图动画延迟'
    },
    legend: {
        data: ['bar', 'bar2']
    },
    toolbox: {
        // y: 'bottom',
        feature: {
            magicType: {
                type: ['stack', 'tiled']
            },
            dataView: {},
            saveAsImage: {
                pixelRatio: 2
            }
        }
    },
    tooltip: {},
    xAxis: {
        data: 'data',
        splitLine: {
            show: false
        }
    },
    yAxis: {
    },
    series: [{
        name: 'bar',
        type: 'bar',
        data: 'data',
        animationDelay: "function(idx){return idx * 10;}"
    }, {
        name: 'bar2',
        type: 'bar',
        data: 'data',
        animationDelay: "function(idx){return idx * 10 + 100;}"
    }],
    animationEasing: 'elasticOut',
    animationDelayUpdate: "function(idx){return idx * 5;}"
}