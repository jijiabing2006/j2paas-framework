﻿{
    xAxis: {
        type: 'category',
        data: 'data'
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: 'data',
        type: 'bar'
    }]
}