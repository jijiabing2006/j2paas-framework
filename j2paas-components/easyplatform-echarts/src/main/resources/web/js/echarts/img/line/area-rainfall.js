{
    title: {
        text: '雨量流量关系图',
        subtext: '数据来自西安兰特水电测控技术有限公司',
        left: 'center',
        align: 'right'
    },
    grid: {
        bottom: 80
    },
    toolbox: {
        feature: {
            dataZoom: {
                yAxisIndex: 'none'
            },
            restore: {},
            saveAsImage: {}
        }
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            animation: false,
            label: {
                backgroundColor: '#505765'
            }
        }
    },
    legend: {
        data: ['流量', '降雨量'],
        left: 10
    },
    dataZoom: [
        {
            show: true,
            realtime: true,
            start: 65,
            end: 85
        },
        {
            type: 'inside',
            realtime: true,
            start: 65,
            end: 85
        }
    ],
    xAxis: [
        {
            type: 'category',
            boundaryGap: false,
            axisLine: {onZero: false},
            data: 'data'
        }
    ],
    yAxis: [
        {
            name: '流量(m^3/s)',
            type: 'value',
            max: 500
        },
        {
            name: '降雨量(mm)',
            nameLocation: 'start',
            max: 5,
            type: 'value',
            inverse: true
        }
    ],
    series: [
        {
            name: '流量',
            type: 'line',
            animation: false,
            areaStyle: {},
            lineStyle: {
                width: 1
            },
            markArea: {
                silent: true,
                data: [[{
                    xAxis: '2009/9/12 7:00'
                }, {
                    xAxis: '2009/9/22 7:00'
                }]]
            },
            data: 'data'
        },
        {
            name: '降雨量',
            type: 'line',
            yAxisIndex: 1,
            animation: false,
            areaStyle: {},
            lineStyle: {
                width: 1
            },
            markArea: {
                silent: true,
                data: [
                    [{
                        xAxis: '2009/9/10 7:00'
                    }, {
                        xAxis: '2009/9/20 7:00'
                    }]
                ]
            },
            data: 'data'
        }
    ]
}