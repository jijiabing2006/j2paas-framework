/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform;

import java.util.Map;

import org.zkoss.json.JSONArray;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkmax.zul.Coachmark;

public class Introcoach extends Coachmark {

	private String actionId;

	private boolean _clearColor;

	public final static String ON_UUID = "onUUID";

	static {
		addClientEvent(Introcoach.class, ON_UUID, 0);
	}

	public void getActionTargetUUID(String targetId){
		actionId = targetId;
		Clients.evalJavaScript("zk.$('"+this.getUuid()+"').getUUID('" +targetId +"')");
	}

	protected void renderProperties(org.zkoss.zk.ui.sys.ContentRenderer renderer)
			throws java.io.IOException {
		super.renderProperties(renderer);

		render(renderer, "clearColor", _clearColor);
		//render(renderer, "site", _site);
	}

	public void service(AuRequest request, boolean everError) {
		final String cmd = request.getCommand();
		final Map data = request.getData();
		if (cmd.equals(ON_UUID)) {
			final JSONArray obj = (JSONArray)data.get("data");
			System.out.println("do onUUID, data:" + obj);
			if (obj != null && obj.size() == 2  && obj.get(0) != null && obj.get(0).equals(this.actionId)) {
				System.out.println("do onUUID, uuid:" + obj.get(1));
				Event evt = new Event(cmd, this, obj.get(1));
				Events.postEvent(evt);
			}
		} else
			super.service(request, everError);
	}

	/**
	 * The default zclass is "z-introcoach"
	 */
	public String getZclass() {
		return (this._zclass != null ? this._zclass : "z-introcoach");
	}

	public boolean isClearColor() {
		return _clearColor;
	}

	public void setClearColor(boolean clearColor) {
		if (this._clearColor != clearColor) {
			this._clearColor = clearColor;
			smartUpdate("clearColor", clearColor);
		}
	}

	/*
	"left-top-aligned"，"top", "right-top-aligned",
	"left", "center"，"right"，
	"left-bottom-aligned","bottom" ,"right-bottom-aligned"
	 */
	public void setSite(String site) {
		Clients.evalJavaScript("zk.$('"+this.getUuid()+"').setSite('" +site +"')");
	}
}

