/* NavigationNode.java

	Purpose:
		
	Description:
		
	History:
		Fri Aug 24 16:25:14 CST 2018, Created by rudyhuang

Copyright (C) 2018 Potix Corporation. All Rights Reserved.
*/
package org.zkoss.zuti.zul;

import java.io.Serializable;

/**
 * The node implementation that used in {@link NavigationModel} internally.
 *
 * @author rudyhuang
 * @since 8.6.0
 */
class NavigationNode<E> implements Serializable {
	private String _key;
	private E _value;
	private NavigationNode<E> _parent;
	private NavigationNode<E> _child;
	private NavigationNode<E> _left;
	private NavigationNode<E> _right;

	NavigationNode() {
	}

	NavigationNode(String key, E value) {
		this._key = key;
		this._value = value;
	}

	public String getKey() {
		return _key;
	}

	public void setKey(String key) {
		this._key = key;
	}

	public E getValue() {
		return _value;
	}

	public void setValue(E value) {
		this._value = value;
	}

	public NavigationNode<E> getParent() {
		return _parent;
	}

	public void setParent(NavigationNode<E> parent) {
		this._parent = parent;
	}

	public NavigationNode<E> getChild() {
		return _child;
	}

	public void setChild(NavigationNode<E> child) {
		this._child = child;
	}

	public NavigationNode<E> getLeft() {
		return _left;
	}

	public void setLeft(NavigationNode<E> left) {
		this._left = left;
	}

	public NavigationNode<E> getRight() {
		return _right;
	}

	public void setRight(NavigationNode<E> right) {
		this._right = right;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("NavigationNode{");
		sb.append("key='").append(_key).append('\'');
		sb.append(", value=").append(_value);
		sb.append('}');
		return sb.toString();
	}
}
