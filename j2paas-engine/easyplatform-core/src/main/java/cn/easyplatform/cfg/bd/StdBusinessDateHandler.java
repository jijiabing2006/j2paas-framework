/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.cfg.bd;

import cn.easyplatform.cfg.BusinessDateHandler;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.type.FieldType;
import org.apache.commons.lang3.time.FastDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class StdBusinessDateHandler implements BusinessDateHandler {

    private CommandContext cc;

    private String query;

    StdBusinessDateHandler(CommandContext cc) {
        this.cc = cc;
        this.query = cc.getProjectService().getConfig().getString("dao.bizDateQuery");
    }

    @Override
    public boolean isBusinessDate(Date date, String ccy) {
        if (date == null)
            return false;
        if (!Strings.isBlank(this.query)) {
            BizDao dao = cc.getBizDao();
            FieldDo param = new FieldDo(FieldType.VARCHAR);
            param.setValue(ccy);
            List<FieldDo> parameter = new ArrayList<FieldDo>();
            parameter.add(param);
            List<FieldDo[]> holidays = dao.selectList(query, parameter);
            if (holidays != null) {
                FastDateFormat df = FastDateFormat.getInstance(cc.getProjectService().getConfig().getDateFormat());
                String inDate = df.format(date);
                for (FieldDo[] holiday : holidays) {
                    String curDate = null;
                    if (holiday[0].getValue() instanceof Date) {
                        curDate = df.format((Date) holiday[0].getValue());
                    } else {
                        curDate = holiday[0].getValue().toString();
                    }
                    //节假日
                    if ("Y".equalsIgnoreCase(holiday[1].getValue().toString()) && curDate.equals(inDate)) {
                        return false;
                    }
                    //特殊工作日
                    if ("N".equalsIgnoreCase(holiday[1].getValue().toString()) && curDate.equals(inDate)) {
                        return true;
                    }
                }
            }
        }
        Calendar calendar = Calendar.getInstance(cc.getProjectService().getLocale());
        //判断是否是周末
        calendar.setTime(date);
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        if (dayOfWeek == Calendar.SUNDAY
                || dayOfWeek == Calendar.SATURDAY) {
            return false;
        }
        return true;
    }

}
