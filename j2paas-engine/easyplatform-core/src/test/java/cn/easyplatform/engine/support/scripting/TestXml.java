/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.support.scripting;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.support.transform.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;

import java.util.List;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TestXml implements TransformerHandler<Map<String, List<TagNode>>> {

	public void test() {
		String xml = Streams.readAndClose(Streams.utf8r(TestParser.class
				.getResourceAsStream("/scripting/XML_MT536_New_031120.XML")));
		Transformer<Document, Map<String, List<TagNode>>> xmlTrans = TransformerFactory
				.createTransformer(1);
		try {
			Document doc = DocumentHelper.parseText(xml);
			xmlTrans.setHandler(this);
			xmlTrans.transform(doc,null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String swi = Streams
				.readAndClose(Streams.utf8r(TestParser.class
						.getResourceAsStream("/scripting/200419337EX543040109.MT543.SWI")));
		Transformer<String, Map<String, List<TagNode>>> swiTrans = TransformerFactory
				.createTransformer(2);
		try {
			swiTrans.setHandler(this);
			swiTrans.transform(swi,null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		Transformer<Workbook, Map<String, List<List<FieldDo>>>> tf = TransformerFactory
				.createTransformer(3);
		tf.setHandler(new TransformerHandler<Map<String, List<List<FieldDo>>>>() {

			@Override
			public void transform(Map<String, List<List<FieldDo>>> data, Parameter parameter) {
				for (Map.Entry<String, List<List<FieldDo>>> entry : data
						.entrySet()) {
					System.out.println("sheet:" + entry.getKey());
					for (List<FieldDo> record : entry.getValue()) {
						System.out.println(record);
					}
				}
			}
		});
		try {
			HSSFWorkbook wb = new HSSFWorkbook(
					TestParser.class
							.getResourceAsStream("/scripting/表结构整理.xls"));
			tf.transform(wb,null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void transform(Map<String, List<TagNode>> data,Parameter parameter) {
		for (Map.Entry<String, List<TagNode>> entry : data.entrySet()) {
			System.out.println(entry.getKey() + "->" + entry.getValue().size());
			// for (TagNode node : entry.getValue()) {
			// if (!node.isLeaf()) {
			// for (TagNode n : node.getChildren()) {
			// if (n.isLeaf())
			// System.out
			// .println(n.getName() + ":" + n.getValue());
			// }
			// }
			// }
			// System.out.println();
		}
	}

}
