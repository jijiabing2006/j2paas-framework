/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao.impl;

import cn.easyplatform.dao.DaoException;
import cn.easyplatform.dao.MessageDao;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DefaultMessageDaoImpl extends AbstractDao implements MessageDao {

	public DefaultMessageDaoImpl(DataSource ds) {
		super(ds);
	}

	@Override
	public List<String[]> selectList(String sql) {
		List<String[]> result = new ArrayList<String[]>();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			int cols = rs.getMetaData().getColumnCount();
			while (rs.next()) {
				String[] astr = new String[cols];
				for (int i = 0; i < cols; i++)
					astr[i] = rs.getString(i + 1);
				result.add(astr);
			}
			return result;
		} catch (SQLException ex) {
			throw new DaoException("dao.biz.query.error", ex,
					"sys_message_info");
		} finally {
			DaoUtils.closeQuietly(stmt, rs);
		}
	}

	@Override
	public String[] select(String sql, String code) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			rs = pstmt.executeQuery();
			if (rs.next()) {
				int cols = rs.getMetaData().getColumnCount();
				String[] astr = new String[cols];
				for (int i = 0; i < cols; i++)
					astr[i] = rs.getString(i + 1);
				return astr;
			}
			return null;
		} catch (SQLException ex) {
			throw new DaoException("dao.biz.query.error", ex,
					"sys_message_info");
		} finally {
			DaoUtils.closeQuietly(pstmt, rs);
		}
	}

	@Override
	public String selectOne(String sql, String code) {
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, code);
			rs = pstmt.executeQuery();
			if (rs.next())
				return rs.getString(1);
			return "";
		} catch (SQLException ex) {
			throw new DaoException("dao.biz.query.error", ex,
					"sys_message_info");
		} finally {
			DaoUtils.closeQuietly(pstmt, rs);
		}
	}

	@Override
	public void deleteAndUpdate(boolean update, boolean drop) {
		if (!update && !drop)
			return;
		PreparedStatement pstmt = null;
		try {
			Connection conn = JdbcTransactions.getConnection(ds);
			if (drop) {
				pstmt = conn
						.prepareStatement("delete from sys_message_info where status='D'");
				pstmt.executeUpdate();
				DaoUtils.closeQuietly(pstmt);
				pstmt = null;
			}
			if (update) {
				pstmt = conn
						.prepareStatement("update sys_message_info set status='R' where status='U' or status='C' or status=' '");
				pstmt.executeUpdate();
				DaoUtils.closeQuietly(pstmt);
				pstmt = null;
			}
		} catch (SQLException ex) {
			throw new DaoException("dao.biz.update.error", ex);
		} finally {
			DaoUtils.closeQuietly(pstmt);
		}
	}

}
