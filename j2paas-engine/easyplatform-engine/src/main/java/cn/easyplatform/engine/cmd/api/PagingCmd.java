/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.api;

import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dao.Page;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.im.PagingRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.h5.MsnVo;
import cn.easyplatform.messages.vos.h5.PageVo;
import cn.easyplatform.messages.vos.h5.PagingVo;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class PagingCmd extends AbstractCommand<PagingRequestMessage> {

    /**
     * @param req
     */
    public PagingCmd(PagingRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        PagingVo pv = req.getBody();
        List<FieldDo> params = new ArrayList<FieldDo>();
        String sql = "select b.sender,b.groupid,b.sendtime,b.title,a.status,a.msgid from sys_notice_detail_info a,sys_notice_info b where a.msgid = b.id and a.touser=? and b.type=?";
        params.add(new FieldDo(FieldType.VARCHAR, cc.getUser().getId()));
        params.add(new FieldDo(FieldType.VARCHAR, pv.getType()));
        if (pv.getStatus() != -1) {
            sql += " and a.status=?";
            params.add(new FieldDo(FieldType.INT, pv.getStatus()));
        }
        Page page = new Page(pv.getPageSize());
        page.setGetTotal(pv.isGetTotal());
        if (pv.getPageNo() == 0)
            page.setPageNo(1);
        else
            page.setPageNo(pv.getPageNo());
        page.setOrderBy("a.msgid desc");
        BizDao dao = cc.getBizDao();
        List<FieldDo[]> objs = dao.selectList(sql, params, page);
        List<MsnVo> result = new ArrayList<MsnVo>(objs.size());
        for (FieldDo[] data : objs) {
            MsnVo mv = new MsnVo((String) data[3].getValue());
            mv.setFromUser((String) data[0].getValue());
            mv.setType(pv.getType());
            Number num = (Number) data[1].getValue();
            mv.setGroupId(num.intValue());
            mv.setTime((Date) data[2].getValue());
            num = (Number) data[4].getValue();
            mv.setStatus(num.intValue());
            num = (Number) data[5].getValue();
            mv.setMsgid(num.longValue());
            result.add(mv);
        }
        MsnVo[] data = new MsnVo[result.size()];
        result.toArray(data);
        result = null;
        objs = null;
        return new SimpleResponseMessage(new PageVo(page.getTotalCount(), data));
    }
}
