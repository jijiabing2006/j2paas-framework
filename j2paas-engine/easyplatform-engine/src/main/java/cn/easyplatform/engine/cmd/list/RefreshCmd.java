/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.list;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.ComponentProcessFactory;
import cn.easyplatform.engine.runtime.DataListProcess;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.ListRefreshRequestMessage;
import cn.easyplatform.messages.response.ListPagingResponseMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListPagingVo;
import cn.easyplatform.messages.vos.datalist.ListRefreshVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RefreshCmd extends AbstractCommand<ListRefreshRequestMessage> {

	/**
	 * @param req
	 */
	public RefreshCmd(ListRefreshRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ListRefreshVo rv = req.getBody();
		ListContext lc = cc.getWorkflowContext().getList(rv.getId());
		if (lc == null)
			return MessageUtils.dataListNotFound(rv.getId());
		if (rv.getKeys() == null) {// 刷新当前页
			DataListProcess dlp = ComponentProcessFactory
					.createDatalistProcess(lc.getType());
			return new ListPagingResponseMessage(dlp.paging(cc,
					new ListPagingVo(rv.getId(), lc.getStartPageNo())));
		} else {
			RecordContext rc = lc.getRecord(rv.getKeys());
			if (rc == null) {
				Record record = DataListUtils.getRecord(cc, lc, rv.getKeys());
				rc = lc.createRecord(rv.getKeys(), record);
				rc.setParameter("814", 'R');
				rc.setParameter("815", Boolean.FALSE);
				lc.appendRecord(rc);
			}
			return new SimpleResponseMessage(DataListUtils.wrapRow(cc, lc, rc));
		}
	}

	@Override
	public String getName() {
		return "list.Refresh";
	}
}
