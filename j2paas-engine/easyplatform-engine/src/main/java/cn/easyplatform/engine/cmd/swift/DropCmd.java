/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.swift;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.SwiftContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.entities.beans.table.TableBean;
import cn.easyplatform.entities.beans.table.TableField;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.messages.request.DropRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.datalist.ListDropVo;
import cn.easyplatform.messages.vos.swift.LinkVo;
import cn.easyplatform.support.sql.SqlParser;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.type.ListRowVo;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class DropCmd extends AbstractCommand<DropRequestMessage> {

	/**
	 * @param req
	 */
	public DropCmd(DropRequestMessage req) {
		super(req);
	}

	@SuppressWarnings("unchecked")
	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		ListDropVo vo = req.getBody();
		WorkflowContext ctx = cc.getWorkflowContext();
		ListContext source = ctx.getList(vo.getSourceId());
		if (source == null)
			return MessageUtils.dataListNotFound(vo.getSourceId());
		SwiftContext sc = ctx.getSwift(vo.getTargetId());
		if (sc == null)
			return MessageUtils.swiftNotFound(vo.getTargetId());
		sc.clear();
		SwiftContext src = ctx.setSwift(vo.getTargetId() + "tmp",
				sc.getTable(), false);
		ListRowVo row = vo.getData().get(0);
		Record record = DataListUtils.getRecord(cc, source,
				source.isCustom() ? row.getData() : row.getKeys());
		RecordContext rc = null;
		if (Lang.equals(ctx.getParameterAsString("833"), source.getBean()
				.getTable())) // 主页面表相同
			ctx.setData(record);
		if (!Strings.isBlank(source.getBeforeLogic())) {// 从原始数据表获取
			String[] keyFields = source.getBeforeLogic().split("\\,");
			if (sc.getTable().getKey().size() != keyFields.length)
				throw new EasyPlatformWithLabelKeyException("dao.biz.key.no.match",
						source.getBeforeLogic(), sc.getTable().getId());
			List<FieldDo> key = new ArrayList<FieldDo>(keyFields.length);
			for (int i = 0; i < keyFields.length; i++) {
				FieldDo fd = record.get(keyFields[i]).clone();
				fd.setName(sc.getTable().getKey().get(i));
				key.add(fd);
			}
			BizDao dao = cc.getBizDao(sc.getTable().getSubType());
			record = dao.selectByKey(sc.getTable(), key);
			if (record == null)
				throw new EasyPlatformWithLabelKeyException(
						"table.record.not.found", sc.getTable().getId(),
						Lang.concat(key.toArray()).toString());
			rc = src.createRecord(record);
		} else
			rc = src.createRecord(record);
		Map<String, LinkVo> info = (Map<String, LinkVo>) vo.getKey();
		src.appendRecord("", null, "", rc, false);
		process(cc, src, info, "", null, rc);
		return new SimpleResponseMessage();
	}

	private void process(CommandContext cc, SwiftContext sc,
			Map<String, LinkVo> info, String parentName, Object[] parentKeys,
			RecordContext parent) {
		for (FieldDo field : parent.getData().getData()) {
			if (field.getDescription().endsWith("#")) {// link
				LinkVo lv = info.get(field.getDescription());
				if (lv != null) {
					TableBean tb = cc.getEntity(lv.getEntity());
					if (tb == null)
						throw new EasyPlatformWithLabelKeyException(
								"entity.not.found", EntityType.TABLE.getName(),
								lv.getEntity());
					BizDao dao = cc.getBizDao(tb.getSubType());
					SqlParser<FieldDo> sp = RuntimeUtils.createSqlParser(FieldDo.class);
					StringBuilder sb = new StringBuilder();
					Iterator<TableField> itr = tb.getFields().iterator();
					sb.append("select ");
					while (itr.hasNext()) {
						TableField tf = itr.next();
						sb.append(tf.getName());
						if (itr.hasNext())
							sb.append(",");
					}
					sb.append(" from ").append(tb.getId())
							.append(" where ").append(lv.getSel());
					String sql = sp.parse(sb.toString(), parent);
					List<FieldDo[]> list = dao.selectList(sql, sp.getParams());
					int size = tb.getFields().size();
					for (FieldDo[] fields : list) {
						Record record = new Record();
						record.setKey(tb.getKey());
						for (int i = 0; i < size; i++) {
							TableField tf = tb.getFields().get(i);
							FieldDo fd = fields[i];
							fd.setDecimal(tf.getDecimal());
							fd.setAcc(tf.getAcc());
							fd.setLength(tf.getLength());
							fd.setDescription(tf.getDescription());
							record.set(fd);
						}
						RecordContext rc = sc.createRecord(record);
						sc.appendRecord(parentName, parentKeys, lv.getName(),
								rc, false);
						process(cc, sc, info, lv.getName(), rc.getKeyValues(),
								rc);
					}
				}// iv linkvo
			}// if#
		}// for
	}
}
