/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.vfs;

import cn.easyplatform.i18n.I18N;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.vfs.ReadRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReadCmd extends AbstractCommand<ReadRequestMessage> {

    /**
     * @param req
     */
    public ReadCmd(ReadRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        String fn = req.getBody();
        if (fn.startsWith("$7")) {
            fn = cc.getRealPath(fn);
            InputStream is = null;
            try {
                File file = new File(fn);
                if (!file.exists())
                    return new SimpleResponseMessage("e026", I18N.getLabel("vfs.dir.not.found", FilenameUtils.getName(fn)));
                if (!file.isFile())
                    return new SimpleResponseMessage("e026", I18N.getLabel("vfs.file.invalid", FilenameUtils.getName(fn)));
                is = new BufferedInputStream(new FileInputStream(file));
                return new SimpleResponseMessage(IOUtils.toByteArray(is));
            } catch (Exception e) {
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", e.getMessage()));
            } finally {
                IOUtils.closeQuietly(is);
            }
        } else {
            File file = new File(req.getBody());
            if (!file.exists())
                return new SimpleResponseMessage("e026", I18N.getLabel("vfs.dir.not.found", FilenameUtils.getName(fn)));
            InputStream is = null;
            try {
                is = new BufferedInputStream(new FileInputStream(file));
                return new SimpleResponseMessage(IOUtils.toByteArray(is));
            } catch (IOException e) {
                return new SimpleResponseMessage("e027", I18N.getLabel("vfs.access.error", e.getMessage()));
            } finally {
                IOUtils.closeQuietly(is);
            }
        }
    }

}
