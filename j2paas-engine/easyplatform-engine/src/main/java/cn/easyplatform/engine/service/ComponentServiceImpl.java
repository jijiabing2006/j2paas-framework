/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.component.*;
import cn.easyplatform.messages.request.*;
import cn.easyplatform.messages.request.addon.SaveListRequestMessage;
import cn.easyplatform.spi.service.ComponentService;
import cn.easyplatform.type.IResponseMessage;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ComponentServiceImpl extends EngineService implements
		ComponentService {

	@Override
	public IResponseMessage<?> getList(GetListRequestMessage req) {
		return commandExecutor.execute(new GetListCmd(req));
	}

	@Override
	public IResponseMessage<?> getCombo(GetComboRequestMessage req) {
		return commandExecutor.execute(new GetComboCmd(req));
	}

	@Override
	public IResponseMessage<?> fieldUpdate(SimpleRequestMessage req) {
		return commandExecutor.execute(new FieldUpdateCmd(req));
	}

	@Override
	public IResponseMessage<?> doReport(DoReportRequestMessage req) {
		return commandExecutor.execute(new DoReportCmd(req));
	}

	@Override
	public IResponseMessage<?> getBpmView(GetBpmViewRequestMessage req) {
		return commandExecutor.execute(new GetBpmViewCmd(req));
	}

	@Override
	public IResponseMessage<?> getBpmTask(GetBpmTaskRequestMessage req) {
		return commandExecutor.execute(new GetBpmTaskCmd(req));
	}

	@Override
	public IResponseMessage<?> getActionValues(
			ActionValueChangedRequestMessage req) {
		return commandExecutor.execute(new GetActionValueCmd(req));
	}

	@Override
	public IResponseMessage<?> upload(UploadRequestMessage req) {
		return commandExecutor.execute(new UploadCmd(req));
	}

	@Override
	public IResponseMessage<?> move(MoveRequestMessage req) {
		return commandExecutor.execute(new MoveCmd(req));
	}

	@Override
	public IResponseMessage<?> doMap(MapRequestMessage req) {
		return commandExecutor.execute(new MapCmd(req));
	}

	@Override
	public IResponseMessage<?> saveList(SaveListRequestMessage req) {
		return commandExecutor.execute(new SaveListCmd(req));
	}
}
