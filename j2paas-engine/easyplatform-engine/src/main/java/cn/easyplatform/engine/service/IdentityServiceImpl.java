/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.service;

import cn.easyplatform.EngineService;
import cn.easyplatform.engine.cmd.application.GetAppEnvCmd;
import cn.easyplatform.engine.cmd.identity.*;
import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.service.IdentityService;
import cn.easyplatform.type.IResponseMessage;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class IdentityServiceImpl extends EngineService implements
        IdentityService {

    @Override
    public IResponseMessage<?> getAppEnv(SimpleRequestMessage req) {
        return commandExecutor.execute(new GetAppEnvCmd(req));
    }

    @Override
    public IResponseMessage<?> login(LoginRequestMessage req) {
        return commandExecutor.execute(new LoginCmd(req));
    }

    @Override
    public IResponseMessage<?> selectOrg(SimpleRequestMessage req) {
        return commandExecutor.execute(new SelectOrgCmd(req));
    }

    @Override
    public IResponseMessage<?> logout(SimpleRequestMessage req) {
        return commandExecutor.execute(new LogoutCmd(req));
    }

    @Override
    public IResponseMessage<?> confirm(SimpleRequestMessage req) {
        return commandExecutor.execute(new ConfirmCmd(req));
    }

    @Override
    public IResponseMessage<?> apiInit(ApiInitRequestMessage req) {
        return commandExecutor.execute(new ApiInitCmd(req));
    }

    @Override
    public IResponseMessage<?> changePassword(ChangePasswordRequestMessage req) {
        return commandExecutor.execute(new ChangePasswordCmd(req));
    }

    @Override
    public IResponseMessage<?> check(CheckRequestMessage req) {
        return commandExecutor.execute(new CheckCmd(req));
    }

    @Override
    public IResponseMessage<?> callback(SimpleRequestMessage req) {
        return commandExecutor.execute(new CallbackCmd(req));
    }
}
