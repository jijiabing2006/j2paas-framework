/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.demo.report;

import cn.easyplatform.EasyPlatformRuntimeException;
import cn.easyplatform.dao.utils.DaoUtils;
import cn.easyplatform.demo.AbstractDemoTest;
import cn.easyplatform.entities.beans.report.JasperReportBean;
import cn.easyplatform.entities.beans.report.JxlsReport;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Streams;
import cn.easyplatform.lang.stream.StringInputStream;
import cn.easyplatform.lang.stream.StringOutputStream;
import cn.easyplatform.transaction.jdbc.JdbcTransactions;
import cn.easyplatform.type.EntityType;
import cn.easyplatform.type.SubType;
import cn.easyplatform.utils.SerializationUtils;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ReportTest extends AbstractDemoTest {

    public void test() throws Exception {
        Connection conn = null;
        PreparedStatement pstmt = null;
        try {
            log.debug("正在新建demo报表");
            conn = JdbcTransactions.getConnection(ds1);

            pstmt = conn.prepareStatement("delete from ep_demo where subType='Jxls'");
            pstmt.executeUpdate();
            pstmt.close();

            pstmt = conn
                    .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
            pstmt.setString(1, "EXCELR001");
            pstmt.setString(2, "简单报表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            String report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("simple.xml")));
            JxlsReport rb = new JxlsReport();
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("simple.xls")));
            StringBuilder sb = new StringBuilder();
            StringOutputStream sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR002");
            pstmt.setString(2, "分组报表(纵向)");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("each_if_demo_1.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("each_if_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR003");
            pstmt.setString(2, "分组报表(横向)");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("each_if_demo_2.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("each_if_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR004");
            pstmt.setString(2, "分组报表(使用变量)");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("each_if_demo_3.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("each_if_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR005");
            pstmt.setString(2, "多组报表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("group_if_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR006");
            pstmt.setString(2, "多表格报表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            //report1 = Streams.readAndClose(Streams.utf8r(getClass()
            //        .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(2);
            //rb.setContent(report1.getBytes());
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("multisheet_markup_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR007");
            pstmt.setString(2, "多单元格合并");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            //report1 = Streams.readAndClose(Streams.utf8r(getClass()
            //        .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            //rb.setContent(report1.getBytes());
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("merged_cells_demo.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR008");
            pstmt.setString(2, "多层扩展");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            //report1 = Streams.readAndClose(Streams.utf8r(getClass()
            //        .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            //rb.setContent(report1.getBytes());
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("formula_copy_template.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR009");
            pstmt.setString(2, "分组报表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            //report1 = Streams.readAndClose(Streams.utf8r(getClass()
            //        .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            //rb.setContent(report1.getBytes());
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("grouping_template.xlsx")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR010");
            pstmt.setString(2, "分页报表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            //report1 = Streams.readAndClose(Streams.utf8r(getClass()
            //        .getResourceAsStream("group_if_demo.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            //rb.setContent(report1.getBytes());
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("paging_template.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.setString(1, "EXCELR011");
            pstmt.setString(2, "订单列表");
            pstmt.setString(3, "");
            pstmt.setString(4, EntityType.REPORT.getName());
            pstmt.setString(5, SubType.JXLS.getName());
            report1 = Streams.readAndClose(Streams.utf8r(getClass()
                    .getResourceAsStream("listbox_template.xml")));
            rb = new JxlsReport();
            rb.setFormulaType(1);
            rb.setConfig(report1);
            rb.setTemplate(Streams.readBytesAndClose(getClass()
                    .getResourceAsStream("listbox_template.xls")));
            sb = new StringBuilder();
            sos = new StringOutputStream(sb);
            TransformerFactory.newInstance().transformToXml(rb, sos);
            pstmt.setString(6, sb.toString());
            pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
            pstmt.setString(8, "admin");
            pstmt.setString(9, "C");
            pstmt.addBatch();

            pstmt.executeBatch();
            pstmt.close();
/**
 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.pie3d.1");
 pstmt.setString(2, "图表与主数据集相同");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report1 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("pie3d_1.jrxml")));
 JasperReportBean rb = new JasperReportBean();
 rb.setContent(report1.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.pie3d.2");
 pstmt.setString(2, "图表与主数据集不同");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report2 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("pie3d_2.jrxml")));
 rb = new JasperReportBean();
 rb.setContent(report2.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.crosstab.1");
 pstmt.setString(2, "交叉表");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report3 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("crosstab_1.jrxml")));
 rb = new JasperReportBean();
 rb.setContent(report3.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.subreport.1");
 pstmt.setString(2, "子表之主表");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report4 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("subreport_1.jrxml")));
 rb = new JasperReportBean();
 rb.setContent(report4.getBytes());
 rb.setQuery("select * from customer");
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.subreport.2");
 pstmt.setString(2, "子表");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report5 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("subreport_2.jrxml")));
 rb = new JasperReportBean();
 rb.setQuery("select * from cust_order where userid=$userid");
 rb.setContent(report5.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.simple.1");
 pstmt.setString(2, "简单报表");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report6 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("simple_1.jrxml")));
 rb = new JasperReportBean();
 rb.setContent(report6.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.simple.2");
 pstmt.setString(2, "主副报表");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report7 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("simple_2.jrxml")));
 rb = new JasperReportBean();
 rb.setContent(report7.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.custom.1");
 pstmt.setString(2, "自定义查询");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report8 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("custom_1.jrxml")));
 rb = new JasperReportBean();
 rb.setQuery("select * from customer");
 EventLogic el = new EventLogic();
 el.setContent("$myvar1=$userid;");
 rb.setOnRecord(el);
 rb.setContent(report8.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();

 pstmt = conn
 .prepareStatement("insert into ep_demo(entityId,name,desp,type,subType,content,createDate,createUser,status) values(?,?,?,?,?,?,?,?,?)");
 pstmt.setString(1, "demo.report.custom.2");
 pstmt.setString(2, "自定义查询");
 pstmt.setString(3, "");
 pstmt.setString(4, EntityType.REPORT.getName());
 pstmt.setString(5, SubType.JASPER.getName());
 String report9 = Streams.readAndClose(Streams.utf8r(getClass()
 .getResourceAsStream("custom_2.jrxml")));
 rb = new JasperReportBean();
 rb.setQuery("select * from cust_order where userid=$userid");
 el = new EventLogic();
 el.setContent("$myvar2=$amount+$myvar2;");
 rb.setOnRecord(el);
 rb.setContent(report9.getBytes());
 report1 = convert(rb);
 pstmt.setString(6, report1);
 pstmt.setTimestamp(7, new Timestamp(System.currentTimeMillis()));
 pstmt.setString(8, "admin");
 pstmt.setString(9, "C");
 pstmt.execute();
 pstmt.close();*/

            log.debug("新建demo报表成功");
        } catch (SQLException ex) {
            throw new EasyPlatformRuntimeException("ReportTest", ex);
        } finally {
            DaoUtils.closeQuietly(pstmt);
        }
    }

    private String convert(JasperReportBean rb) throws Exception {
        StringInputStream is = new StringInputStream(
                new String(rb.getContent()));
        JasperReport jr = JasperCompileManager.compileReport(is);
        rb.setSerialize(SerializationUtils.serialize(jr));
        StringBuilder sb = new StringBuilder();
        StringOutputStream sos = new StringOutputStream(sb);
        TransformerFactory.newInstance().transformToXml(rb, sos);
        return sb.toString();
    }
}
