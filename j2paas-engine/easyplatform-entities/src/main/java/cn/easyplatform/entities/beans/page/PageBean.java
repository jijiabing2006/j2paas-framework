/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.page;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.EventLogic;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"inheritId", "width", "height", "table", "onLoad",
        "onBack", "onOk", "onRefresh", "onVisible", "script", "ajax",
        "mil", "destination", "onMessage", "onShow", "ajaxDevice", "milDevice", "apply"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "page")
public class PageBean extends BaseEntity implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -1180444436173818590L;

    @XmlElement
    private String inheritId;// 继承上一个PageBean的id

    @XmlElement
    private int width;// 宽度

    @XmlElement
    private int height;// 高度

    @XmlElement
    private String table;// 对应table的entity id

    @XmlElement
    private String destination;//要订阅消息的名称

    @XmlElement
    private String onMessage;//消息到达事件

    //////////////////后台逻辑//////////////////////
    @XmlElement
    private EventLogic onLoad;// 初始化逻辑

    @XmlElement
    private EventLogic onBack;// 返回逻辑

    @XmlElement
    private EventLogic onOk;// 确定逻辑

    @XmlElement
    private EventLogic onRefresh;// 刷新或确认逻辑

    // /////////////////前台逻辑////////////////////////
    @XmlElement
    private String onVisible;// 页面在客户端生成后调用的逻辑，主要是控制页面组件的状态，列如只读、必输等选项

    @XmlElement
    private String script;// 定义脚本方法，定义方法后可以由页面组件使用

    @XmlElement
    private String onShow;//在显示之前运行脚本，一般在这里绑定变量

    /*
     * 以下栏位是2种终端页面内容
     */
    @XmlElement
    private String ajax;// web web页面内容

    @XmlElement
    private String mil;// 移动设备

    @XmlElement
    private DeviceBean ajaxDevice;

    @XmlElement
    private DeviceBean milDevice;

    @XmlElement  //自定义实现类
    private String apply;

    public DeviceBean getAjaxDevice() {
        return ajaxDevice;
    }

    public void setAjaxDevice(DeviceBean ajaxDevice) {
        this.ajaxDevice = ajaxDevice;
    }

    public DeviceBean getMilDevice() {
        return milDevice;
    }

    public void setMilDevice(DeviceBean milDevice) {
        this.milDevice = milDevice;
    }

    public String getOnShow() {
        return onShow;
    }

    public void setOnShow(String onShow) {
        this.onShow = onShow;
    }

    public String getInheritId() {
        return inheritId;
    }

    public void setInheritId(String inheritId) {
        this.inheritId = inheritId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public EventLogic getOnLoad() {
        return onLoad;
    }

    public void setOnLoad(EventLogic onLoad) {
        this.onLoad = onLoad;
    }

    public EventLogic getOnBack() {
        return onBack;
    }

    public void setOnBack(EventLogic onBack) {
        this.onBack = onBack;
    }

    public EventLogic getOnOk() {
        return onOk;
    }

    public void setOnOk(EventLogic onOk) {
        this.onOk = onOk;
    }

    public EventLogic getOnRefresh() {
        return onRefresh;
    }

    public void setOnRefresh(EventLogic onRefresh) {
        this.onRefresh = onRefresh;
    }

    public String getOnVisible() {
        return onVisible;
    }

    public void setOnVisible(String onVisible) {
        this.onVisible = onVisible;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }

    public String getAjax() {
        return ajax;
    }

    public void setAjax(String ajax) {
        this.ajax = ajax;
    }

    public String getMil() {
        return mil;
    }

    public void setMil(String mil) {
        this.mil = mil;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getOnMessage() {
        return onMessage;
    }

    public void setOnMessage(String onMessage) {
        this.onMessage = onMessage;
    }

    public String getApply() {
        return apply;
    }

    public void setApply(String apply) {
        this.apply = apply;
    }

    @Override
    public PageBean clone() {
        try {
            return (PageBean) super.clone();
        } catch (Exception ex) {
        }
        return null;
    }
}
