/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.project;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"name", "loginPage", "mainPage", "loginId", "mainId"})
@XmlAccessorType(XmlAccessType.NONE)
public class DeviceMapBean implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @XmlAttribute(required = true)
    private String type;

    @XmlAttribute
    private String theme;

    @XmlElement(required = true)
    private String name;

    @XmlElement
    private String loginPage;

    @XmlElement
    private String mainPage;

    @XmlElement
    private long loginId;

    @XmlElement
    private long mainId;

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the loginPage
     */
    public String getLoginPage() {
        return loginPage;
    }

    /**
     * @param loginPage the loginPage to set
     */
    public void setLoginPage(String loginPage) {
        this.loginPage = loginPage;
    }

    /**
     * @return the mainPage
     */
    public String getMainPage() {
        return mainPage;
    }

    /**
     * @param mainPage the mainPage to set
     */
    public void setMainPage(String mainPage) {
        this.mainPage = mainPage;
    }

    public long getLoginId() {
        return loginId;
    }

    public void setLoginId(long loginId) {
        this.loginId = loginId;
    }

    public long getMainId() {
        return mainId;
    }

    public void setMainId(long mainId) {
        this.mainId = mainId;
    }
}
