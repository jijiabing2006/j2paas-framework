/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.task;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.EventLogic;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"inheritId", "refId", "table", "processCode", "image", "executeType",
        "isUpdatable", "isVisible", "onInit", "onClose", "onPreCommit",
        "onPreCommitRollback", "onBeforeCommit", "onAfterCommit", "onRollback",
        "onCommitted", "onCommittedRollback", "variables", "decision", "access", "accessType"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "task")
public class TaskBean extends BaseEntity implements Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -4396669497642947312L;

    @XmlElement
    private String inheritId;// 继承上一个TaskBean的id

    @XmlElement(required = true)
    private String refId;// 引用可执行参数的id

    @XmlElement
    private String table;

    @XmlElement
    private String processCode = "C";// CRUD

    @XmlElement(required = true)
    private String image;// 显示的图标路径,必须在客户端已存在的目录下,例如 webapp/images/,规格16*16

    @XmlElement
    private boolean isUpdatable = true;// 是否可以写数据

    @XmlElement
    private boolean isVisible = true;// 是否可视

    @XmlElement
    private EventLogic onInit;// 开始逻辑，如果在NAVI流程中，可以执行功能与功能之间的数据映射

    @XmlElement
    private EventLogic onClose;// 关闭页面逻辑,是指功能不是提交关闭，而是通过触发关闭按钮来中断功能
    /*
     * 第1阶段提交
     */
    @XmlElement
    private EventLogic onPreCommit;// 在主数据提交之前事件

    @XmlElement
    private EventLogic onPreCommitRollback;// 在主数据提交之前失败事件
    /*
     * 第2阶段提交
     */
    @XmlElement
    private EventLogic onBeforeCommit;// 数据提交之前逻辑

    @XmlElement
    private EventLogic onAfterCommit;// 数据提交之后逻辑

    @XmlElement
    private EventLogic onRollback;// 失败时调用逻辑
    /*
     * 第3阶段提交
     */
    @XmlElement
    private EventLogic onCommitted;// 数据提交成功之后逻辑

    @XmlElement
    private EventLogic onCommittedRollback;// 数据提交成功之后逻辑时调用逻辑

    @XmlElement
    private DecisionBean decision;// 功能流程

    @XmlElement(name = "variable")
    private List<Variable> variables;// 变量声明

    @XmlElement
    private Integer executeType;//执行类型, 804变量

    @XmlElement
    private String access;

    @XmlElement
    private Integer accessType;// 0或空表示仅允许管理中心访问 1.表示登陆后在任何页面访问 2.表示允许匿名访问

    public Integer getAccessType() {
        return accessType;
    }

    public void setAccessType(Integer accessType) {
        this.accessType = accessType;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public Integer getExecuteType() {
        return executeType;
    }

    public void setExecuteType(Integer executeType) {
        this.executeType = executeType;
    }

    public String getInheritId() {
        return inheritId;
    }

    public void setInheritId(String inheritId) {
        this.inheritId = inheritId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public boolean isUpdatable() {
        return isUpdatable;
    }

    public void setUpdatable(boolean isUpdatable) {
        this.isUpdatable = isUpdatable;
    }

    public boolean isVisible() {
        return isVisible;
    }

    public void setVisible(boolean isVisible) {
        this.isVisible = isVisible;
    }

    public EventLogic getOnBeforeCommit() {
        return onBeforeCommit;
    }

    public void setOnBeforeCommit(EventLogic onBeforeCommit) {
        this.onBeforeCommit = onBeforeCommit;
    }

    public EventLogic getOnAfterCommit() {
        return onAfterCommit;
    }

    public void setOnAfterCommit(EventLogic onAfterCommit) {
        this.onAfterCommit = onAfterCommit;
    }

    public EventLogic getOnRollback() {
        return onRollback;
    }

    public void setOnRollback(EventLogic onRollback) {
        this.onRollback = onRollback;
    }

    public EventLogic getOnInit() {
        return onInit;
    }

    public void setOnInit(EventLogic onInit) {
        this.onInit = onInit;
    }

    public EventLogic getOnClose() {
        return onClose;
    }

    public void setOnClose(EventLogic onClose) {
        this.onClose = onClose;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public DecisionBean getDecision() {
        return decision;
    }

    public void setDecision(DecisionBean decision) {
        this.decision = decision;
    }

    /**
     * @return the onPreCommit
     */
    public EventLogic getOnPreCommit() {
        return onPreCommit;
    }

    /**
     * @param onPreCommit the onPreCommit to set
     */
    public void setOnPreCommit(EventLogic onPreCommit) {
        this.onPreCommit = onPreCommit;
    }

    /**
     * @return the onPreCommitRollback
     */
    public EventLogic getOnPreCommitRollback() {
        return onPreCommitRollback;
    }

    /**
     * @param onPreCommitRollback the onPreCommitRollback to set
     */
    public void setOnPreCommitRollback(EventLogic onPreCommitRollback) {
        this.onPreCommitRollback = onPreCommitRollback;
    }

    /**
     * @return the onCommitted
     */
    public EventLogic getOnCommitted() {
        return onCommitted;
    }

    /**
     * @param onCommitted the onCommitted to set
     */
    public void setOnCommitted(EventLogic onCommitted) {
        this.onCommitted = onCommitted;
    }

    /**
     * @return the onCommittedRollback
     */
    public EventLogic getOnCommittedRollback() {
        return onCommittedRollback;
    }

    /**
     * @param onCommittedRollback the onCommittedRollback to set
     */
    public void setOnCommittedRollback(EventLogic onCommittedRollback) {
        this.onCommittedRollback = onCommittedRollback;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    @Override
    public TaskBean clone() {
        try {
            return (TaskBean) super.clone();
        } catch (Exception ex) {
        }
        return null;
    }

    @Override
    public String toString() {
        return refId;
    }

}
