/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GuestTaskVo extends TaskVo {

    /**
     *
     */
    private static final long serialVersionUID = -3118506048589325625L;

    private String deviceType;

    private String serviceId;

    private String ip;

    public GuestTaskVo(String id, String deviceType, String serviceId) {
        super(id);
        this.deviceType = deviceType;
        this.serviceId = serviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getServiceId() {
        return serviceId;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}
