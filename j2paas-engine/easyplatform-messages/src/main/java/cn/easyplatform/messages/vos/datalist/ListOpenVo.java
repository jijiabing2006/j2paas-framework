/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListOpenVo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

	private Object[] keys;

	private String pageId;

	private int openModel;

	private String processCode;

	private Object[] copyKeys;

	public String getId() {
		return id;
	}

	public void setId(String listId) {
		this.id = listId;
	}

	public Object[] getKeys() {
		return keys;
	}

	public void setKeys(Object[] keys) {
		this.keys = keys;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public int getOpenModel() {
		return openModel;
	}

	public void setOpenModel(int openModel) {
		this.openModel = openModel;
	}

	public String getProcessCode() {
		return processCode;
	}

	public void setProcessCode(String processCode) {
		this.processCode = processCode;
	}

	/**
	 * @return the copyKeys
	 */
	public Object[] getCopyKeys() {
		return copyKeys;
	}

	/**
	 * @param copyKeys
	 *            the copyKeys to set
	 */
	public void setCopyKeys(Object[] copyKeys) {
		this.copyKeys = copyKeys;
	}

}
