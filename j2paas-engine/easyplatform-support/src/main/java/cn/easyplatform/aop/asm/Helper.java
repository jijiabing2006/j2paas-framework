/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.asm;

/**
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 * 
 */
public final class Helper {

	public static byte valueOf(Byte value) {
		if (value == null)
			return 0;
		return value.byteValue();
	}

	public static short valueOf(Short value) {
		if (value == null)
			return 0;
		return value.shortValue();
	}

	public static int valueOf(Integer value) {
		if (value == null)
			return 0;
		return value.intValue();
	}

	public static long valueOf(Long value) {
		if (value == null)
			return 0;
		return value.longValue();
	}

	public static double valueOf(Double value) {
		if (value == null)
			return 0;
		return value.doubleValue();
	}

	public static float valueOf(Float value) {
		if (value == null)
			return 0;
		return value.floatValue();
	}

	public static boolean valueOf(Boolean value) {
		if (value == null)
			return false;
		return value.booleanValue();
	}

	public static char valueOf(Character value) {
		if (value == null)
			return 0;
		return value.charValue();
	}

}
