/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.jms;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ConnectionKey {

    private final String userName;
    private final String password;
    private int hash;

    public ConnectionKey(String userName, String password) {
        this.password = password;
        this.userName = userName;
        hash = 31;
        if (userName != null) {
            hash += userName.hashCode();
        }
        hash *= 31;
        if (password != null) {
            hash += password.hashCode();
        }
    }

    @Override
    public int hashCode() {
        return hash;
    }

    @Override
    public boolean equals(Object that) {
        if (this == that) {
            return true;
        }
        if (that instanceof ConnectionKey) {
            return equals((ConnectionKey) that);
        }
        return false;
    }

    public boolean equals(ConnectionKey that) {
        return isEqual(this.userName, that.userName) && isEqual(this.password, that.password);
    }

    public String getPassword() {
        return password;
    }

    public String getUserName() {
        return userName;
    }

    public static boolean isEqual(Object o1, Object o2) {
        if (o1 == o2) {
            return true;
        }
        return o1 != null && o2 != null && o1.equals(o2);
    }
}
