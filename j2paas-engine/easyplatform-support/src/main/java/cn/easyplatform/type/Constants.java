/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class Constants {

    // 数据列表应用类型
    public static final String CATALOG = "catalog";

    public static final String REPORT = "report";

    public static final String DETAIL = "detail";

    public static final String ACTIONBOX = "actionbox";

    public static final String LOADER = "loader";

    public static final String HIERARCHY = "hierarchy";

    // 页面打开方式
    public final static int OPEN_NORMAL = 0;

    public final static int OPEN_NEW_PAGE = 1;

    public final static int OPEN_MODAL = 2;

    public final static int OPEN_OVERLAPPED = 3;

    public final static int OPEN_EMBBED = 4;

    public final static int OPEN_POPUP = 5;

    // 逻辑名称

    public final static String ON_INIT = "onInit";

    public final static String ON_CLOSE = "onClose";

    public final static String ON_PRE_COMMIT = "onPreCommit";

    public final static String ON_PRE_COMMIT_ROLLBACK = "onPreCommitRollback";

    public final static String ON_BEFORE_COMMIT = "onBeforeCommit";

    public final static String ON_AFTER_COMMIT = "onAfterCommit";

    public final static String ON_ROLLBACK = "onRollback";

    public final static String ON_COMMITTED = "onCommitted";

    public final static String ON_COMMITTED_ROLLBACK = "onCommittedRollback";

    public final static String ON_LOAD = "onLoad";

    public final static String ON_REFRESH = "onRefresh";

    public final static String ON_OK = "onOk";

    public final static String ON_BACK = "onBack";

    public final static String SESSION_ID = "$serverSessionId";

    public final static String EASYPLATFORM_TOPIC = "easyplatformTopic";
}
