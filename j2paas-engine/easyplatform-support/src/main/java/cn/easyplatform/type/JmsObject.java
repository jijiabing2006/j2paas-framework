/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.type;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class JmsObject {

    private String messageID;

    private long timeToLive;

    private long timestamp;

    private Object object;

    public JmsObject(String messageID, long timestamp, long timeToLive, Object object) {
        this.messageID = messageID;
        this.timeToLive = timeToLive;
        this.timestamp = timestamp;
        this.object = object;
    }

    public void setTimeToLive(long timeToLive) {
        this.timeToLive = timeToLive;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimeToLive() {
        return timeToLive;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getMessageID() {
        return messageID;
    }

    public Object getObject() {
        return object;
    }
}
