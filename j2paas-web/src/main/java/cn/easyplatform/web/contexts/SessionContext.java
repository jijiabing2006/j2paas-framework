/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.contexts;

import cn.easyplatform.lang.util.AbstractContext;
import cn.easyplatform.lang.util.Context;
import org.zkoss.zk.ui.Session;

import java.util.Set;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class SessionContext extends AbstractContext {

    private Session sess;

    /**
     * @param sess
     */
    public SessionContext(Session sess) {
        this.sess = sess;
    }

    @Override
    public Context set(String name, Object value) {
        sess.setAttribute(name, value);
        return this;
    }

    @Override
    public Set<String> keys() {
        return sess.getAttributes().keySet();
    }

    @Override
    public boolean has(String key) {
        return sess.hasAttribute(key);
    }

    @Override
    public Context clear() {
        return this;
    }

    @Override
    public int size() {
        return sess.getAttributes().size();
    }

    @Override
    public Object get(String name) {
        return sess.getAttribute(name);
    }

    @Override
    public Object remove(String name) {
        return sess.removeAttribute(name);
    }
}
