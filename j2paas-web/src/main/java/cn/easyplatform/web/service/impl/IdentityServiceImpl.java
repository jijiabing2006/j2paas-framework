/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.service.impl;

import cn.easyplatform.messages.request.*;
import cn.easyplatform.spi.engine.EngineFactory;
import cn.easyplatform.spi.service.IdentityService;
import cn.easyplatform.type.IResponseMessage;


/**
 * 不能直接使用，只能通过WebProxyFactory来获取代理
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class IdentityServiceImpl implements IdentityService {

	private IdentityService broker = EngineFactory.me().getEngineService(
			IdentityService.class);

	@Override
	public IResponseMessage<?> login(LoginRequestMessage req) {
		return broker.login(req);
	}

	@Override
	public IResponseMessage<?> selectOrg(SimpleRequestMessage req) {
		return broker.selectOrg(req);
	}

	@Override
	public IResponseMessage<?> logout(SimpleRequestMessage req) {
		return broker.logout(req);
	}

	@Override
	public IResponseMessage<?> getAppEnv(SimpleRequestMessage req) {
		return broker.getAppEnv(req);
	}

	@Override
	public IResponseMessage<?> confirm(SimpleRequestMessage req) {
		return broker.confirm(req);
	}

	@Override
	public IResponseMessage<?> apiInit(ApiInitRequestMessage req) {
		return broker.apiInit(req);
	}

	@Override
	public IResponseMessage<?> changePassword(ChangePasswordRequestMessage req) {
		return broker.changePassword(req);
	}

	@Override
	public IResponseMessage<?> check(CheckRequestMessage req) {
		return broker.check(req);
	}

	@Override
	public IResponseMessage<?> callback(SimpleRequestMessage req) {
		return broker.callback(req);
	}
}
