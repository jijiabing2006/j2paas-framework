/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.exporter;

import cn.easyplatform.messages.vos.datalist.ListHeaderVo;
import cn.easyplatform.web.task.zkex.ListSupport;
import cn.easyplatform.web.task.OperableHandler;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Tree;
import org.zkoss.zul.Treecol;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
class TreeExportImpl extends AbstractExporter {

    private Tree listbox;

    /**
     * @param main
     * @param support
     */
    TreeExportImpl(OperableHandler main, ListSupport support) {
        super(main, support);
        listbox = (Tree) support.getComponent();
    }

    @Override
    protected ListModel<?> createHeadersModel() {
        List<ListHeaderVo> headers = new ArrayList<ListHeaderVo>();
        for (Component c : listbox.getTreecols().getChildren()) {
            Treecol header = (Treecol) c;
            ListHeaderVo hv = (ListHeaderVo) header.getAttribute("value");
            if (header.isVisible() && hv != null)
                headers.add(hv);
        }
        ListModelList<ListHeaderVo> lm = new ListModelList<ListHeaderVo>(
                headers, true);
        lm.setMultiple(true);
        return lm;
    }

}
