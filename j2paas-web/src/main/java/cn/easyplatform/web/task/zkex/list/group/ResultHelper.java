/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.zkex.list.group;

import cn.easyplatform.type.ListRowVo;

import java.util.Iterator;
import java.util.List;


/**
 * @author <a href="mailto:shiny@rj-it.com">shiny</a>
 * @since Nov 23, 2012
 */
public class ResultHelper {

    private Iterator<ListRowVo> itr;

    private ListRowVo currentItem;

    /**
     * @param data
     */
    public ResultHelper(List<ListRowVo> data) {
        itr = data.iterator();
    }

    /**
     * @return
     */
    public boolean hasNext() {
        return itr.hasNext();
    }

    /**
     *
     */
    public ListRowVo next() {
        return currentItem = itr.next();
    }

    public ListRowVo item() {
        return currentItem;
    }
}
