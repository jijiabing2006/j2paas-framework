/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class CookieUtils {

    // cookie的有效期默认为30天
    public final static int COOKIE_MAX_AGE = 60 * 60 * 24 * 30;

    /**
     * 添加一个新Cookie
     *
     * @param response HttpServletResponse
     * @param cookie   新cookie
     * @return null
     */
    public static void addCookie(HttpServletResponse response, Cookie cookie) {
        if (cookie != null)
            response.addCookie(cookie);
    }

    /**
     * 添加一个新Cookie
     *
     * @param response    HttpServletResponse
     * @param cookieName  cookie名称
     * @param cookieValue cookie值
     * @param domain      cookie所属的子域
     * @param httpOnly    是否将cookie设置成HttpOnly
     * @param maxAge      设置cookie的最大生存期
     * @param path        设置cookie路径
     * @param secure      是否只允许HTTPS访问
     * @return null
     */
    public static void addCookie(HttpServletResponse response, String cookieName, String cookieValue, String domain,
                                 boolean httpOnly, int maxAge, String path, boolean secure) {
        if (cookieName != null && !cookieName.equals("")) {
            if (cookieValue == null)
                cookieValue = "";

            Cookie newCookie = null;
            try {
                newCookie = new Cookie(cookieName, URLEncoder.encode(cookieValue, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                newCookie = new Cookie(cookieName, cookieValue);
            }
            if (domain != null)
                newCookie.setDomain(domain);

            newCookie.setHttpOnly(httpOnly);

            if (maxAge > 0)
                newCookie.setMaxAge(maxAge);

            if (path == null)
                newCookie.setPath("/");
            else
                newCookie.setPath(path);

            newCookie.setSecure(secure);

            addCookie(response, newCookie);
        }
    }

    /**
     * 添加一个新Cookie
     *
     * @param response    HttpServletResponse
     * @param cookieName  cookie名称
     * @param cookieValue cookie值
     * @param domain      cookie所属的子域
     * @return null
     */
    public static void addCookie(HttpServletResponse response, String cookieName, String cookieValue, String domain) {
        addCookie(response, cookieName, cookieValue, domain, true, COOKIE_MAX_AGE, "/", false);
    }

    /**
     * 根据Cookie名获取对应的Cookie
     *
     * @param request    HttpServletRequest
     * @param cookieName cookie名称
     * @return 对应cookie，如果不存在则返回null
     */
    public static Cookie getCookie(HttpServletRequest request, String cookieName) {
        Cookie[] cookies = request.getCookies();

        if (cookies == null || cookieName == null || cookieName.equals(""))
            return null;

        for (Cookie c : cookies) {
            if (c.getName().equals(cookieName))
                return c;
        }
        return null;
    }

    /**
     * 根据Cookie名获取对应的Cookie值
     *
     * @param request    HttpServletRequest
     * @param cookieName cookie名称
     * @return 对应cookie值，如果不存在则返回null
     */
    public static String getCookieValue(HttpServletRequest request, String cookieName) {
        Cookie cookie = getCookie(request, cookieName);
        if (cookie == null)
            return null;
        else {
            try {
                return URLDecoder.decode(cookie.getValue(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                return cookie.getValue();
            }
        }
    }

    /**
     * 删除指定Cookie
     *
     * @param response HttpServletResponse
     * @param cookie   待删除cookie
     */
    public static void delCookie(HttpServletResponse response, Cookie cookie) {
        if (cookie != null) {
            cookie.setPath("/");
            cookie.setMaxAge(0);
            cookie.setValue(null);

            response.addCookie(cookie);
        }
    }

    /**
     * 根据cookie名删除指定的cookie
     *
     * @param request    HttpServletRequest
     * @param response   HttpServletResponse
     * @param cookieName 待删除cookie名
     */
    public static void delCookie(HttpServletRequest request, HttpServletResponse response, String cookieName) {
        Cookie c = getCookie(request, cookieName);
        if (c != null && c.getName().equals(cookieName)) {
            delCookie(response, c);
        }
    }

    /**
     * 根据cookie名修改指定的cookie
     *
     * @param request     HttpServletRequest
     * @param response    HttpServletResponse
     * @param cookieName  cookie名
     * @param cookieValue 修改之后的cookie值
     * @param domain      修改之后的domain值
     */
    public static void editCookie(HttpServletRequest request, HttpServletResponse response, String cookieName,
                                  String cookieValue, String domain) {
        Cookie c = getCookie(request, cookieName);
        if (c != null && cookieName != null && !cookieName.equals("") && c.getName().equals(cookieName)) {
            addCookie(response, cookieName, cookieValue, domain);
        }
    }
}
